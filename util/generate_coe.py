#!/usr/bin/env python
# -*- coding: utf-8 -*-

from sys import argv
from pprint import pprint

# Margem em volta do padrao
margin = (2,2)

outName = "../init.coe"
if len(argv) < 2:
	print(u"""Argumentos insuficientes!
		Uso: ./generate_coe.py <arquivo> <../init.coe>
		<arquivo> é o arquivo fonte contendo o padrão de inicialização.
		O segundo argumento é o arquivo de saida, se for omitido assume-se
		que seja "../init.coe".""")
	exit(1)
elif len(argv) > 3:
	outName = argv[2]

inName = argv[1]

rawgrid = set()
grid = set()
minX = 0
minY = 0
maxX = 0
maxY = 0
with open(inName, 'r') as infile:
	for line in infile:
		if line[0] != '#':
			x,y = line.split()
			x = int(x)
			y = int(y)
			minX = min(minX, x)
			minY = min(minY, y)
			maxX = max(maxX, x)
			maxY = max(maxY, y)
			rawgrid.add( (x,y) )
	for cell in rawgrid:
		grid.add( (cell[0]-minX+margin[0], cell[1]-minY+margin[1]) )

# pprint( grid)
# print("Bounding X :",minX, maxX)
with open(outName, 'w') as outfile:
	matrix = [ [0]*5 for x in xrange((maxX-minX + margin[0]+1)) ]
	# pprint(matrix)
	outfile.write("memory_initialization_radix=16;\nmemory_initialization_vector=")
	g = sorted(grid)
	for cell in g:
		bit = cell[1]%32
		block = cell[1]/32
		# print(cell, block, bit)
		matrix[cell[0]][block] |= (1<<bit)
	for a in matrix:
		for l in a:
			outfile.write('{:08X}\n'.format(l))
	# pprint(matrix)
		