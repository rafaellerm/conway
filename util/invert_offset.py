#!/usr/bin/env python
# -*- coding: utf-8 -*-

from sys import argv

invert_x = True
invert_y = True
offset = (0, 78)

in_name = ''
if len(argv) < 2:
	exit("Unsufficient arguments")

out_name = 'modified_pattern.lif'
if len(argv) > 2:
	out_name = argv[2]

with open(argv[1], 'r') as in_file:
	with open(out_name, 'w') as out_file:
		for line in in_file:
			if line[0] == '#':
				out_file.write(line)
			else:
				x,y = [int(a) for a in line.split()]
				if invert_x:
					x=-x
				if invert_y:
					y=-y
				x = x+offset[0]
				y = y+offset[1]
				out_file.write('{:d} {:d}\n'.format(x, y))