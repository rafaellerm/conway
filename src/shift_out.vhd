-- Shift register com:
-- empty
-- load
-- Shift

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity shift_out is
	generic (
		data_w : natural := 32
	);
    port (
	    clk, reset : in std_logic;

	    empty : out std_logic;
	    d_out : out std_logic;
	    
	    shift : in std_logic;
	    data_in : in std_logic_vector(data_w-1 downto 0);
	    load, load_zero : in std_logic
    );
end entity; -- shift_out

architecture arch of shift_out is
	signal reg : std_logic_vector(data_w-1 downto 0);
begin
	process (clk, reset)
		variable n_dados : integer range 0 to data_w+1 := 0;
	begin
		if rising_edge(clk) then
			if reset = '1' then
				reg <= (others => '0');
			elsif load = '1' then
				reg <= data_in;
				n_dados := data_w;
			elsif load_zero='1' then
				reg <= (others => '0');
				n_dados := data_w;
			elsif shift = '1' then
				reg <= '0' & reg(data_w-1 downto 1);
				n_dados := n_dados -1;
			end if;
			if n_dados = 0 then
				empty <= '1';
			else
				empty <= '0';
			end if;
		end if;
	end process;

	d_out <= reg(0);
end architecture; -- arch