library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.conway_pack.all;

entity conway is
	generic (
		n_lines : natural := 120;
		n_cols : natural := 5;
		addr_w : natural := 10;
		data_w : natural := 32
	);
    port (
	    clk, reset, enable : in std_logic;

	    clk_read : in std_logic;
	    display_addr : in std_logic_vector(addr_w-1 downto 0);
	    display_data : out std_logic_vector(data_w-1 downto 0);

	    last_line_out : out std_logic;
	    active_buffer_out : out std_logic
    );
end entity; -- conway

architecture arch of conway is
	signal line : unsigned(ilog2(n_lines)-1 downto 0) := to_unsigned(0, ilog2(n_lines));
	signal column : unsigned(ilog2(n_cols)-1 downto 0) := to_unsigned(0, ilog2(n_cols));
	signal write_ptr, curr_ptr, prev_ptr, next_ptr : unsigned(addr_w -1 downto 0);

	signal read_blocks_empty_v : std_logic_vector(2 downto 0);
	signal read_blocks_empty : std_logic;

	signal read_ptr_src : read_addr_src_t;
	signal read_ptr : unsigned(addr_w-1 downto 0);

	signal active_buffer : std_logic;
	signal change_active_buffer : std_logic;

	signal write_enable : std_logic;
	signal read_data, write_data : std_logic_vector(data_w-1 downto 0);

	signal reset_regs, shift_regs, shift_grid, shift_grid_zero, shift_output, reset_ouput : std_logic;
	signal prev_bit, curr_bit, next_bit : std_logic;
	signal load_prev_reg, load_curr_reg, load_next_reg : std_logic;
	signal load_zero_prev, load_zero_next : std_logic;
	signal incr_curr_ptr, incr_write_ptr : std_logic;
	signal reset_temp, reset_temp2 : std_logic;

	signal first_line, first_column, last_line, last_column : std_logic;
	signal incr_line, incr_column, reset_line, reset_column : std_logic;
	signal output_full : std_logic;

	signal prev_grid, curr_grid, next_grid : std_logic_vector(2 downto 0);

	signal cell_life : std_logic;
begin
	last_line_out <= last_line;
	active_buffer_out <= active_buffer;

	prev_ptr <= curr_ptr - n_cols;
	next_ptr <= curr_ptr + n_cols;

	read_blocks_empty <= or_reduce(read_blocks_empty_v);

	with read_ptr_src select read_ptr <=
		prev_ptr when prev_p,
		curr_ptr when curr_p,
		next_ptr when next_p,
		(others => 'X') when others;

	clk_p : process( clk, reset, change_active_buffer )
	begin
		if rising_edge(clk) then
			if reset='1' then
				active_buffer <= '1';
				line <= to_unsigned(0, line'length);
				column <= to_unsigned(0, column'length);
				curr_ptr <= to_unsigned(0, curr_ptr'length);
				write_ptr <= to_unsigned(0, write_ptr'length);
				prev_grid <= (others => '0');
				curr_grid <= (others => '0');
				next_grid <= (others => '0');
			else
				if change_active_buffer='1' then
					active_buffer <= not active_buffer;
				end if;
				if incr_curr_ptr='1' then
					curr_ptr <= curr_ptr+1;
				end if;
				if incr_write_ptr='1' then
					write_ptr <= write_ptr+1;
				end if;
				if reset_line='1' then
					line <= to_unsigned(0, line'length);
				elsif incr_line='1' then
					line <= line+1;
				end if;
				if reset_column='1' then
					column <= to_unsigned(0, column'length);
				elsif incr_column='1' then
					column <= column+1;
				end if;
				if reset_regs='1' then
					prev_grid <= (others => '0');
					curr_grid <= (others => '0');
					next_grid <= (others => '0');

					curr_ptr <= to_unsigned(0, curr_ptr'length);
					write_ptr <= to_unsigned(0, write_ptr'length);
				elsif shift_grid='1' then
					if shift_grid_zero='1' then
						prev_grid(0) <= '0';
						curr_grid(0) <= '0';
						next_grid(0) <= '0';
					else
						prev_grid(0) <= prev_bit;
						curr_grid(0) <= curr_bit;
						next_grid(0) <= next_bit;
					end if;

					prev_grid(2 downto 1) <= prev_grid(1 downto 0);
					curr_grid(2 downto 1) <= curr_grid(1 downto 0);
					next_grid(2 downto 1) <= next_grid(1 downto 0);
				end if;
			end if;
		end if;
	end process; -- clk_p

	count_p : process( prev_grid, curr_grid, next_grid )
		variable neighbors : std_logic_vector(7 downto 0);
		variable neighbor_count : integer;
	begin
		neighbors := prev_grid & curr_grid(2) & curr_grid(0) & next_grid;
		neighbor_count := count_ones(neighbors);
		-- Regras do jogo
		cell_life <= '0';
		if curr_grid(1)='1' then
			if neighbor_count=0 or neighbor_count=1 then
				cell_life <= '0';
			elsif neighbor_count=2 or neighbor_count=3 then
				cell_life <= '1';
			end if;
		elsif neighbor_count=3 then
			cell_life <= '1';
		end if;
	end process; -- count_p

	first_line <= '1' when line=0 else '0';
	last_line <= '1' when line=n_lines-1 else '0';
	first_column <= '1' when column=0 else '0';
	last_column <= '1' when column=n_cols-1 else '0';

	mem_inst : entity work.memory_selector
	PORT MAP(
		clk0 => clk,
		clk1 => clk_read,
		active_bank => active_buffer,
		write_enable => write_enable,
		addr0 => std_logic_vector(read_ptr),
		addr1 => display_addr,
		write_addr => std_logic_vector(write_ptr),
		data0 => read_data,
		data1 => display_data,
		write_data => write_data
	);

	reset_temp <= reset or reset_regs;

	prev_reg_inst : entity work.shift_out
	generic map(
		data_w => data_w
	)
	PORT MAP(
		clk => clk,
		reset => reset_temp,
		empty => read_blocks_empty_v(0),
		d_out => prev_bit,
		shift => shift_regs,
		data_in => read_data,
		load => load_prev_reg,
		load_zero => load_zero_prev
	);

	curr_reg_inst : entity work.shift_out
	generic map(
		data_w => data_w
	)
	PORT MAP(
		clk => clk,
		reset => reset_temp,
		empty => read_blocks_empty_v(1),
		d_out => curr_bit,
		load_zero => '0',
		shift => shift_regs,
		data_in => read_data,
		load => load_curr_reg
	);

	next_reg_inst : entity work.shift_out
	generic map(
		data_w => data_w
	)
	PORT MAP(
		clk => clk,
		reset => reset_temp,
		empty => read_blocks_empty_v(2),
		d_out => next_bit,
		shift => shift_regs,
		data_in => read_data,
		load => load_next_reg,
		load_zero => load_zero_next
	);

	reset_temp2 <= reset or reset_ouput;
	
	out_reg_inst : entity work.shift_in 
	PORT MAP(
		clk => clk,
		reset => reset_temp2,
		full => output_full,
		d_out => write_data,
		shift => shift_output,
		data_in => cell_life
	);


	control_inst : entity work.conway_control
    port map(
	    clk => clk,
	    reset => reset,
	    enable => enable,

	    read_blocks_empty => read_blocks_empty,
	    first_line => first_line,
	    first_column => first_column,
	    last_line => last_line,
	    last_column => last_column,
	    output_full => output_full,
	
		read_ptr_src => read_ptr_src,
		incr_line => incr_line,
		incr_column => incr_column,
		reset_line => reset_line,
		reset_column => reset_column,
		reset_regs => reset_regs,
		shift_regs => shift_regs,
		shift_output => shift_output,
		shift_grid_zero => shift_grid_zero,
		shift_grid => shift_grid,
		reset_ouput => reset_ouput,
		load_prev_reg => load_prev_reg,
		load_curr_reg => load_curr_reg,
		load_next_reg => load_next_reg,
		load_zero_prev => load_zero_prev,
		load_zero_next => load_zero_next,
		incr_curr_ptr => incr_curr_ptr,
		incr_write_ptr => incr_write_ptr,
	    change_active_buffer => change_active_buffer,
	    write_enable => write_enable
    );

end architecture; -- arch