library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_misc.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_textio.all;
use std.textio.all;

entity ram_quad is
   generic (
   		memory_type : string := "DEFAULT";
   		file_name : string := "src/code.txt";
   		WORD_W : natural := 40
   		-- Memoria enderecada a palavra
   		ADDR_W : natural := 9
   		);
   port(
   		clk               : in std_logic;
        enable            : in std_logic;

        address0,
        address1,         : in std_logic_vector(ADDR_W-1 downto 0);
        write0_enable     : in std_logic;

        data_write        : in std_logic_vector(WORD_W-1 downto 0);
        data0_read,
        data1_read        : out std_logic_vector(WORD_W-1 downto 0)
        );
end; --entity ram_quad

architecture logic of ram_quad is
   constant ADDRESS_WIDTH   : natural := ADDR_W;
begin

   generic_ram:
   if memory_type /= "ALTERA_LPM" generate 
   begin
   --Simulate a synchronous RAM
   ram_proc: process(clk, enable, write_byte_enable, 
         address, data_write) --mem_write, mem_sel
      variable mem_size : natural := 2 ** ADDRESS_WIDTH;
      variable data : std_logic_vector(WORD_W-1 downto 0); 
      subtype word is std_logic_vector(WORD_W-1 downto 0);
      type storage_array is
         array(natural range 0 to mem_size/4 - 1) of word;
      variable storage : storage_array := (others => (others => 'U'));
      variable index : natural := 0;
      file load_file : text open read_mode is file_name;
      variable hex_file_line : line;

      variable data1 : std_logic_vector(WORD_W-1 downto 0);
      variable index1 : natural := 0;
   begin

      --Load in the sram executable image
      if index = 0 then
         while not endfile(load_file) loop
--The following two lines had to be commented out for synthesis
            readline(load_file, hex_file_line);
            hread(hex_file_line, data);
            storage(index) := data;
            index := index + 1;
         end loop;
      end if;

      if rising_edge(clk) then
         index := to_integer(unsigned(address0)); -- conv_integer(address(ADDRESS_WIDTH-1 downto 2));
         data := storage(index);

         if enable = '1' then
            if write0_enable = '1' then
               data := data_write;
            end if;
         end if;
      
         index1 := to_integer(unsigned(address1));
         data1 := storage(index1);

         if write0_enable /= '0' then
            storage(index) := data;
         end if;

      end if;

      data0_read <= data;
      data1_read <= data1;
   end process;
   end generate; --generic_ram

end; --architecture logic
