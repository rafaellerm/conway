library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.conway_pack.all;

entity conway_control is
    port (
	    clk, reset, enable : in std_logic;

	    read_blocks_empty : in std_logic;
	    first_line, first_column, last_line, last_column : in std_logic;
	    output_full : in std_logic;
	
		read_ptr_src : out read_addr_src_t;
		incr_line, incr_column, reset_line, reset_column : out std_logic;
		reset_regs, shift_regs, shift_output, shift_grid, shift_grid_zero, reset_ouput : out std_logic;
		load_prev_reg, load_curr_reg, load_next_reg : out std_logic;
		load_zero_prev, load_zero_next : out std_logic;
		incr_curr_ptr, incr_write_ptr : out std_logic;
	    change_active_buffer : out std_logic;
	    write_enable : out std_logic
    );
end entity; -- conway_control

architecture arch of conway_control is
	type process_state_t is (
		initial, 
		first_cell_in_block_0, 
		first_cell_in_block_1,
		process_block,
		write_block,
		process_last_element,
		process_last_element_1
		);
	signal process_state : process_state_t := initial;
	signal next_process_state : process_state_t;

	type fetch_state_t is (
		initial,
		fetch_prev,
		fetch_curr,
		fetch_next,
		save_next
		);
	signal fetch_state : fetch_state_t := initial;
	signal next_fetch_state : fetch_state_t;
begin

	process( clk, reset )
	begin
		if rising_edge(clk) then
			if reset='1' then
				process_state <= initial;
				fetch_state <= initial;
			elsif enable='1' then
			-- else
				process_state <= next_process_state;
				fetch_state <= next_fetch_state;
			end if;
		end if;
	end process;

	process( process_state, fetch_state,
				read_blocks_empty,
	    		first_line, first_column, last_line, last_column,
	    		output_full, enable )
	begin
		next_process_state <= process_state;
		next_fetch_state <= fetch_state;
		read_ptr_src <= prev_p;
		incr_line <= '0';
		incr_column <= '0';
		reset_line <= '0';
		reset_column <= '0';
		reset_regs <= '0';
		shift_regs <= '0';
		shift_output <= '0';
		shift_grid <= '0';
		reset_ouput <= '0';
		load_prev_reg <= '0';
		load_curr_reg <= '0';
		load_next_reg <= '0';
		load_zero_prev <= '0';
		load_zero_next <= '0';
		incr_curr_ptr <= '0';
		incr_write_ptr <= '0';
	    change_active_buffer <= '0';
	    write_enable <= '0';
	    shift_grid_zero <= '0';

		case( process_state ) is
			when initial =>
				-- reset_line <= '1';
				-- reset_column <= '1';
				reset_ouput <= '1';
				-- reset_regs <= '1';
				if read_blocks_empty='0' then
					next_process_state <= first_cell_in_block_0;
				end if;
			when first_cell_in_block_0 =>
				-- incr_column <= '1'; -- Aqui?
				shift_grid <= '1';
				shift_regs <= '1';
				next_process_state <= first_cell_in_block_1;
			when first_cell_in_block_1 =>
				shift_grid <= '1';
				shift_regs <= '1';
				next_process_state <= process_block;
			when process_block =>
				if output_full='0' then
					if read_blocks_empty='0' then
						shift_grid <= '1';
						shift_output <= '1';
						shift_regs <= '1';
					elsif last_column='1' then
						next_process_state <= process_last_element;
					end if;
				else
					next_process_state <= write_block;
				end if;
			when write_block =>
				write_enable <= '1';
				reset_ouput <= '1';
				incr_write_ptr <= '1';
				if last_column='1' then 
					next_process_state <= initial; 
				else
					next_process_state <= process_block;
				end if;
				-- Teste
				if last_column='1' then
					if last_line='1' then
						reset_line <= '1';
						reset_regs <= '1';
						change_active_buffer <= '1';
					else 
						incr_line <= '1';
					end if;
					reset_column <= '1';
				else
					incr_column <= '1';
				end if;
			when process_last_element =>
				shift_grid <= '1';
				shift_output <= '1';
				shift_grid_zero <= '1'; -- teste
				next_process_state <= process_last_element_1;
			when process_last_element_1 =>
				shift_grid <= '1';
				shift_output <= '1';
				shift_grid_zero <= '1'; -- teste
				-- Espera-se que output_full va ser 0 no proximo ciclo
				next_process_state <= write_block;

		end case;
	-- end process;

	-- process( fetch_state,
	-- 			read_blocks_empty,
	--     		first_line, first_column, last_line, last_column,
	--     		output_full )
	-- begin
		case( fetch_state ) is
			when initial =>
				next_fetch_state <= fetch_prev;
			when fetch_prev =>
				if read_blocks_empty='1' then
					read_ptr_src <= prev_p;
					next_fetch_state <= fetch_curr;
				end if;
			when fetch_curr =>
				if first_line='0' or (first_line='1' and last_column='1') then
					load_prev_reg <= '1';
				else
					load_zero_prev <= '1';
				end if;
				read_ptr_src <= curr_p;
				next_fetch_state <= fetch_next;
			when fetch_next =>
				load_curr_reg <= '1';
				read_ptr_src <= next_p;
				next_fetch_state <= save_next;
			when save_next =>
				if last_line='0' then
					load_next_reg <= '1';
				else
					load_zero_next <= '1';
				end if;
				incr_curr_ptr <= '1';
				if last_line='1' then
					next_fetch_state <= initial;
				else
					next_fetch_state <= fetch_prev;
				end if;

		end case;
	end process;
end architecture; -- arch