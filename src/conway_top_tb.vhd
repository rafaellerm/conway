--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   21:12:15 07/15/2013
-- Design Name:   
-- Module Name:   C:/Users/Rafael/Documents/Ufrgs/Projeto_e_Teste_de_Sistemas_VLSI/conway/ise_proj/conway_top_tb.vhd
-- Project Name:  conway
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: conway_top
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY conway_top_tb IS
END conway_top_tb;
 
ARCHITECTURE behavior OF conway_top_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT conway_top
    PORT(
         mclk : IN  std_logic;
         btn_step : IN  std_logic;
         btn_reset : IN  std_logic;
         HSYNC : OUT  std_logic;
         VSYNC : OUT  std_logic;
         red : OUT  std_logic_vector(2 downto 0);
         green : OUT  std_logic_vector(2 downto 0);
         blue : OUT  std_logic_vector(2 downto 1);
         game_enable_out : OUT  std_logic;
         active_buffer_out : OUT  std_logic;
         sw : IN  std_logic_vector(7 downto 0);
         step_clk_out : OUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal mclk : std_logic := '0';
   signal btn_step : std_logic := '0';
   signal btn_reset : std_logic := '0';
   signal sw : std_logic_vector(7 downto 0) := (1 downto 0=>'1', others => '0');

 	--Outputs
   signal HSYNC : std_logic;
   signal VSYNC : std_logic;
   signal red : std_logic_vector(2 downto 0);
   signal green : std_logic_vector(2 downto 0);
   signal blue : std_logic_vector(2 downto 1);
   signal game_enable_out : std_logic;
   signal active_buffer_out : std_logic;
   signal step_clk_out : std_logic;

   -- Clock period definitions
   constant mclk_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: conway_top PORT MAP (
          mclk => mclk,
          btn_step => btn_step,
          btn_reset => btn_reset,
          HSYNC => HSYNC,
          VSYNC => VSYNC,
          red => red,
          green => green,
          blue => blue,
          game_enable_out => game_enable_out,
          active_buffer_out => active_buffer_out,
          sw => sw,
          step_clk_out => step_clk_out
        );

   -- Clock process definitions
   mclk_process :process
   begin
		mclk <= '0';
		wait for mclk_period/2;
		mclk <= '1';
		wait for mclk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 100 ns;	

      wait for mclk_period*10;

      -- insert stimulus here 

      wait;
   end process;

END;
