--
--	Package File Template
--
--	Purpose: This package defines supplemental types, subtypes, 
--		 constants, and functions 
--
--   To use any of the example code shown below, uncomment the lines and modify as necessary
--

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.math_real.all;
use IEEE.NUMERIC_STD.ALL;

package conway_pack is

	type read_addr_src_t is (prev_p, curr_p, next_p);


	function ilog2 (constant x : in integer) return integer;
	function vectorize(s: std_logic) return std_logic_vector;
	function itoslv(s: integer; dest : std_logic_vector) return std_logic_vector;
	function or_reduce(arg : std_logic_vector) return std_logic;
	function to_std_logic(b : boolean) return std_logic;
	function count_ones(slv : std_logic_vector) return natural;
end conway_pack;

package body conway_pack is
	function ilog2 (constant x : in integer) return integer is
	begin
		return integer(ceil(log2(real(x))));
	end ilog2;

	function vectorize(s: std_logic) return std_logic_vector is
		variable v: std_logic_vector(0 downto 0);
	begin
		v(0) := s;
		return v;
	end;

	function itoslv(s: integer; dest : std_logic_vector) return std_logic_vector is
	begin
		return std_logic_vector(to_unsigned(s, dest'length));
	end itoslv;

	function or_reduce (arg : std_logic_vector )
	return std_logic is
		variable Upper, Lower : std_logic;
		variable Half : integer;
		variable BUS_int : std_logic_vector ( arg'length - 1 downto 0 );
		variable Result : std_logic;
	begin
		if (arg'LENGTH < 1) then            -- In the case of a NULL range
			Result := '0';
		else
			BUS_int := to_ux01 (arg);
			if ( BUS_int'length = 1 ) then
				Result := BUS_int ( BUS_int'left );
			elsif ( BUS_int'length = 2 ) then
				Result := BUS_int ( BUS_int'right ) or BUS_int ( BUS_int'left );
			else
				Half := ( BUS_int'length + 1 ) / 2 + BUS_int'right;
				Upper := or_reduce ( BUS_int ( BUS_int'left downto Half ));
				Lower := or_reduce ( BUS_int ( Half - 1 downto BUS_int'right ));
				Result := Upper or Lower;
			end if;	
		end if;
		return Result;
	end;

	function to_std_logic(b : boolean) return std_logic is
	begin
		if b = true then
			return '1';
		else
			return '0';
		end if;
	end;
 
	function count_ones(slv : std_logic_vector) return natural is
		variable n_ones : natural := 0;
	begin
		for i in slv'range loop
			if slv(i) = '1' then
				n_ones := n_ones + 1;
			end if;
		end loop;
		return n_ones;
	end function count_ones;

end conway_pack;
