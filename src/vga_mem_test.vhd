library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;

entity vga_mem_test is
    port (
	    mclk : in std_logic;
	    --btn_change : in std_logic;

	    HSYNC, VSYNC : out std_logic;

	    red, green : out std_logic_vector(2 downto 0);
	    blue : out std_logic_vector(2 downto 1)
    );
end entity; -- vga_mem_test

architecture arch of vga_mem_test is
	signal clk_25MHz : std_logic;
	signal blank : std_logic;
	signal hcount, vcount : std_logic_vector(10 downto 0);

	signal red_s, green_s : std_logic_vector(2 downto 0);
	signal blue_s : std_logic_vector(2 downto 1);

	signal active_bank : std_logic;

	signal mem_addr : std_logic_vector(9 downto 0);
	signal mem_data: std_logic_vector(31 downto 0);
	signal addr_in_w : std_logic_vector(4 downto 0);
	signal pixel : std_logic;

	COMPONENT clk_divider
	PORT(
		CLKIN_IN : IN std_logic;
		RST_IN : IN std_logic;          
		CLKDV_OUT : OUT std_logic;
		CLKIN_IBUFG_OUT : OUT std_logic;
		CLK0_OUT : OUT std_logic;
		LOCKED_OUT : OUT std_logic
		);
	END COMPONENT;

begin

	vga_control_inst : entity work.vga_controller_640_60 
	PORT MAP(
		rst => '0',
		pixel_clk => clk_25MHz,
		HS => HSYNC,
		VS => VSYNC,
		hcount => hcount,
		vcount => vcount,
		blank => blank
	);

	active_bank <= '0';

	mem_inst : entity work.memory_selector 
	PORT MAP(
		clk0 => '0',
		clk1 => clk_25MHz,
		active_bank => active_bank,
		write_enable => '0',
		addr0 => (9 downto 0 => '0'),
		addr1 => mem_addr,
		write_addr => (9 downto 0 => '0'),
		data0 => open,
		data1 => mem_data,
		write_data => (31 downto 0 => '0')
	);

	mem_addr <= vcount(9 downto 2) & hcount(8 downto 7);
	addr_in_w <= hcount(6 downto 2);
	pixel <= mem_data(to_integer(unsigned(addr_in_w)));

	-- vcount and (not 3) -- bloco do inicio da linha
	-- hcount srl 7 -- bloco dentro da linha

	red_s <= (others => '0') when pixel = '1' else (others => '1');
	green_s <= (others => '0') when pixel = '1' else (others => '1');
	blue_s <= "11";
	
	red <= red_s when blank='0' else (others => '0');
	green <= green_s when blank='0' else (others => '0');
	blue <= blue_s when blank='0' else (others => '0');

	divider_inst: clk_divider 
	PORT MAP(
		CLKIN_IN => mclk,
		RST_IN => '0',
		CLKDV_OUT => clk_25MHz,
		CLKIN_IBUFG_OUT => open,
		CLK0_OUT => open,
		LOCKED_OUT => open
	);
end architecture; -- arch