library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity shift_in is
	generic (
		data_w : natural := 32
	);
    port (
	    clk, reset : in std_logic;
	    
	    full : out std_logic;
	    d_out : out std_logic_vector(data_w-1 downto 0);

	    shift : in std_logic;
	    data_in : in std_logic
    );
end entity; -- shift_in

architecture arch of shift_in is
	signal reg : std_logic_vector(data_w-1 downto 0);
begin
	process (clk, reset)
		variable n_dados : integer range 0 to data_w+1 := 0;
	begin
		if rising_edge(clk) then
			if reset='1' then
				reg <= (others => '0');
				n_dados := 0;
			elsif shift='1' then
				reg(reg'left) <= data_in;
				reg(reg'left-1 downto 0) <= reg(reg'left downto 1);
				n_dados := n_dados+1;
			end if;
			if n_dados=data_w then
				full <= '1';
			else
				full <= '0';
			end if;
		end if;
	end process;
	d_out <= reg;
end architecture; -- arch