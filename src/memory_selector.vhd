library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.conway_pack.all;

entity memory_selector is
	generic (
		data_w: natural := 32;
		addr_w: natural := 10
	);
    port (
    	clk0, clk1 : in std_logic;
	    active_bank : in std_logic;

	    write_enable : in std_logic;
	   	addr0, addr1, write_addr : in std_logic_vector(addr_w-1 downto 0);
	   	data0, data1 : out std_logic_vector(data_w-1 downto 0);
	   	write_data : in std_logic_vector(data_w-1 downto 0)
    );
end entity; -- memory_selector

architecture arch of memory_selector is
	COMPONENT ram
		PORT (
			clka : IN STD_LOGIC;
			wea : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
			addra : IN STD_LOGIC_VECTOR(addr_w-1 DOWNTO 0);
			dina : IN STD_LOGIC_VECTOR(data_w-1 DOWNTO 0);
			douta : OUT STD_LOGIC_VECTOR(data_w-1 DOWNTO 0);
			clkb : IN STD_LOGIC;
			web : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
			addrb : IN STD_LOGIC_VECTOR(addr_w-1 DOWNTO 0);
			dinb : IN STD_LOGIC_VECTOR(data_w-1 DOWNTO 0);
			doutb : OUT STD_LOGIC_VECTOR(data_w-1 DOWNTO 0)
		);
	END COMPONENT;

	signal wea, web : std_logic;
	signal a_doutb, b_doutb, a_douta, b_douta : std_logic_vector(data_w-1 downto 0);
	signal a_addra, b_addra : std_logic_vector(addr_w-1 downto 0);
begin
	ra_inst: ram
	PORT MAP (
		clka => clk0,
		wea => vectorize(wea),
		addra => a_addra,
		dina => write_data,
		douta => a_douta,
		clkb => clk1,
		web => vectorize('0'),
		addrb => addr1,
		dinb => (data_w-1 DOWNTO 0=>'0'),
		doutb => a_doutb
	);

	rb_inst: ram
	PORT MAP (
		clka => clk0,
		wea => vectorize(web),
		addra => b_addra,
		dina => write_data,
		douta => b_douta,
		clkb => clk1,
		web => vectorize('0'),
		addrb => addr1,
		dinb => (data_w-1 DOWNTO 0=>'0'),
		doutb => b_doutb
	);

	data1 <= a_doutb when active_bank = '0' else b_doutb;
	data0 <= a_douta when active_bank = '0' else b_douta;

	a_addra <= addr0 when active_bank = '0' else write_addr;
	b_addra <= addr0 when active_bank = '1' else write_addr;

	wea <= write_enable when active_bank = '1' else '0';
	web <= write_enable when active_bank = '0' else '0';
end architecture; -- arch