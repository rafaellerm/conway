library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.conway_pack.all;

entity conway_top is
    port (
	    mclk : in std_logic;
	    btn_step : in std_logic;
	    btn_reset : in std_logic;

	    HSYNC, VSYNC : out std_logic;

	    red, green : out std_logic_vector(2 downto 0);
	    blue : out std_logic_vector(2 downto 1);

	    game_enable_out : out std_logic;
	    active_buffer_out : out std_logic;

	    sw : in std_logic_vector(7 downto 0);
	    step_clk_out, led_locked : out std_logic
    );
end entity; -- conway_top

architecture arch of conway_top is
	signal clk_25MHz : std_logic;
	signal blank : std_logic;
	signal hcount, vcount : std_logic_vector(10 downto 0);

	signal red_s, green_s : std_logic_vector(2 downto 0);
	signal blue_s : std_logic_vector(2 downto 1);

	signal mem_addr : std_logic_vector(9 downto 0);
	signal mem_addr_u : unsigned(mem_addr'range);
	signal mem_data: std_logic_vector(31 downto 0);
	signal addr_in_w, addr_in_w_r : std_logic_vector(4 downto 0);
	signal pixel : std_logic;
	signal game_enable, game_clk, last_line : std_logic;
	signal clk : std_logic;

	signal HSYNC_r, VSYNC_r : std_logic;
    signal red_r, green_r : std_logic_vector(2 downto 0);
    signal blue_r : std_logic_vector(2 downto 1);

    signal step_clk_counter : unsigned(21 downto 0) := (others=>'0');
    signal step_clk : std_logic;

	COMPONENT clk_divider
	PORT(
		CLKIN_IN : IN std_logic;
		RST_IN : IN std_logic;          
		CLKDV_OUT : OUT std_logic;
		CLKIN_IBUFG_OUT : OUT std_logic;
		CLK0_OUT : OUT std_logic;
		LOCKED_OUT : OUT std_logic
		);
	END COMPONENT;
begin
	vga_control_inst : entity work.vga_controller_640_60 
	PORT MAP(
		rst => btn_reset,
		pixel_clk => clk_25MHz,
		HS => HSYNC_r,
		VS => VSYNC_r,
		hcount => hcount,
		vcount => vcount,
		blank => blank
	);

	game_inst : entity work.conway
	port map (
	    clk => game_clk,
	    reset => btn_reset,
	    enable => game_enable,

	    clk_read => clk_25MHz,
	    display_addr => mem_addr,
	    display_data => mem_data,
	    last_line_out => last_line,
	    active_buffer_out => active_buffer_out
    );

	process (vcount, hcount)
		variable v_i : integer;
		variable h_i : integer;
	begin
		v_i := to_integer(shift_right(unsigned(vcount), 2));
		h_i := to_integer(shift_right(unsigned(hcount), 7));

		mem_addr_u <= to_unsigned((v_i*5) + h_i, mem_addr_u'length);
	end process;
	
	mem_addr <= std_logic_vector(mem_addr_u);
	addr_in_w_r <= hcount(6 downto 2);
	pixel <= mem_data(to_integer(unsigned(addr_in_w)));

	red_s <= (others => '0') when pixel = '1' else (others => '1');
	green_s <= (others => '0') when pixel = '1' else (others => '1');
	blue_s <= "11";
	
	red_r <= red_s when blank='0' else (others => '0');
	green_r <= green_s when blank='0' else (others => '0');
	blue_r <= blue_s when blank='0' else (others => '0');

	divider_inst: clk_divider 
	PORT MAP(
		CLKIN_IN => mclk,
		RST_IN => '0',
		CLKDV_OUT => clk_25MHz,
		CLKIN_IBUFG_OUT => open,
		CLK0_OUT => clk,
		LOCKED_OUT => led_locked
	);

	process( game_clk, sw )
	begin
		if rising_edge(game_clk) then
			step_clk_counter <= step_clk_counter + 1;
		end if;
	end process;
	step_clk <=
		step_clk_counter(step_clk_counter'left-4) when sw(4)='1' else
		step_clk_counter(step_clk_counter'left-3) when sw(3)='1' else
		step_clk_counter(step_clk_counter'left-2) when sw(2)='1' else
		step_clk_counter(step_clk_counter'left-1) when sw(1)='1' else
		step_clk_counter(step_clk_counter'left) when sw(0)='1' else
		'0';
	step_clk_out <= step_clk;

	process( clk_25MHz, btn_step, sw )
		variable old_btn : std_logic := '0';
		variable run_step : std_logic := '0';
		variable old_step_clk : std_logic := '0';
	begin
		if rising_edge(clk_25MHz) then
			if (old_btn='0' and btn_step='1') or (step_clk='1' and old_step_clk='0') then
				run_step := '1';
			end if;

			old_step_clk := step_clk;
			old_btn := btn_step;
			game_enable <= '0';
			if run_step='1' and last_line='0' then
				game_enable <= '1';
			elsif run_step='1' and last_line='1' then
				run_step := '0';
			end if;
			if last_line='1' then
				game_enable <= '1';
			end if;
		end if;
	end process;

	game_enable_out <= game_enable;
	process( clk )
		variable clk_counter : unsigned(1 downto 0);
	begin
		if rising_edge(clk) then
			clk_counter := clk_counter + 1;
			game_clk <= clk_counter(clk_counter'left);
		end if;
	end process;

	process( clk_25MHz )
	begin
		if rising_edge(clk_25MHz) then
			addr_in_w <= addr_in_w_r;

			HSYNC <= HSYNC_r;
			VSYNC <= VSYNC_r;
			red <= red_r;
			green <= green_r;
			blue <= blue_r;
		end if;
	end process;
end architecture; -- arch