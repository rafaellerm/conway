--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   00:51:30 07/12/2013
-- Design Name:   
-- Module Name:   C:/Users/Rafael/Documents/Ufrgs/Projeto_e_Teste_de_Sistemas_VLSI/conway/ise_proj/conway_tb.vhd
-- Project Name:  conway
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: conway
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY conway_tb IS
END conway_tb;
 
ARCHITECTURE behavior OF conway_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT conway
    PORT(
         clk : IN  std_logic;
         reset : IN  std_logic;
         enable : IN  std_logic;
         clk_read : IN  std_logic;
         display_addr : IN  std_logic_vector(9 downto 0);
         display_data : OUT  std_logic_vector(31 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal clk : std_logic := '0';
   signal reset : std_logic := '0';
   signal enable : std_logic := '0';
   signal clk_read : std_logic := '0';
   signal display_addr : std_logic_vector(9 downto 0) := (others => '0');

 	--Outputs
   signal display_data : std_logic_vector(31 downto 0);

   -- Clock period definitions
   constant clk_period : time := 20 ns;
   constant clk_read_period : time := 20 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: conway PORT MAP (
          clk => clk,
          reset => reset,
          enable => enable,
          clk_read => clk_read,
          display_addr => display_addr,
          display_data => display_data
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 
   clk_read_process :process
   begin
		clk_read <= '0';
		wait for clk_read_period/2;
		clk_read <= '1';
		wait for clk_read_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      reset <= '1';
      wait for 100 ns;	
	reset <= '0';
      wait for clk_period*10;

      enable <= '1';

      wait;
   end process;

END;
