library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity conway_cell_logic is
    port (
	    line0, line1, line2 : in std_logic_vector(2 downto 0);
	    active_line : in std_logic_vector(1 downto 0);

	    cell_life: out std_logic
    );
end entity; -- conway_cell_logic

architecture arch of conway_cell_logic is
	signal neighbors : std_logic_vector(7 downto 0);
	signal active_cell : std_logic;

	function count_ones(slv : std_logic_vector) return natural is
	  variable n_ones : natural := 0;
	begin
	  for i in slv'range loop
	    if slv(i) = '1' then
	      n_ones := n_ones + 1;
	    end if;
	  end loop;

	  return n_ones;
	end function count_ones;

begin
	with active_line select neighbors <=
		(line0(2) & line0(0) & line1 & line2) when "00",
		(line1(2) & line1(0) & line2 & line0) when "01",
		(line2(2) & line2(0) & line0 & line1) when "10",
		(others => 'X') when others;

	with active_line select active_cell <=
		line0(1) when "00",
		line1(1) when "01",
		line2(1) when "10",
		'X' when others;

	out_p : process( line0, line1, line2, active_line )
		variable neighbor_count : natural := 0;
	begin
		neighbor_count := count_ones(neighbors);
		cell_life <= '0';
		if active_cell = '1' then
			if neighbor_count = 0 or neighbor_count = 1 then
				-- Menos de dois vizinhos vivos, celula morre
				cell_life <= '0';
			elsif neighbor_count = 2 or neighbor_count = 3 then
				-- 2 ou 3 vizihnos vivos, celula sobrevive
				cell_life <= '1';
			else
				-- Mais do que tres vizinhos vivos, celula morre
				cell_life <= '0';
			end if;
		elsif neighbor_count = 3 then
			-- Celula morta com exatamente tres vizinhos -> nasce
			cell_life <= '1';
		end if;
	end process; -- out_p
end architecture; -- arch